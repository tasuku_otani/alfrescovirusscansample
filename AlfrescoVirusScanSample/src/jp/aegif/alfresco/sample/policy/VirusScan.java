package jp.aegif.alfresco.sample.policy;

import java.util.ArrayList;
import java.util.List;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.ContentServicePolicies;
import org.alfresco.repo.content.ContentServicePolicies.OnContentUpdatePolicy;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.repo.transaction.AlfrescoTransactionSupport;
import org.alfresco.repo.transaction.TransactionListenerAdapter;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

public class VirusScan extends TransactionListenerAdapter implements OnContentUpdatePolicy, InitializingBean {

    private static final String NODE_KEY = "VirusScan.TARGET_NODE_REF";
    private static final Log LOGGER = LogFactory.getLog(VirusScan.class);

    private PolicyComponent policyComponent;
    private NodeService nodeService;
    private ActionService actionService;

    public void setPolicyComponent(PolicyComponent policyComponent) {
        this.policyComponent = policyComponent;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.policyComponent.bindClassBehaviour(ContentServicePolicies.OnContentUpdatePolicy.QNAME, this, new JavaBehaviour(this, "onContentUpdate"));
    }

    @Override
    public void onContentUpdate(NodeRef nodeRef, boolean newContent) {
        if (!nodeService.getType(nodeRef).equals(ContentModel.TYPE_CONTENT) || nodeService.getProperty(nodeRef, ContentModel.PROP_CONTENT) == null) {
            return;
        }
        List<NodeRef> nodeRefs = AlfrescoTransactionSupport.getResource(NODE_KEY);
        if (nodeRefs == null) {
            nodeRefs = new ArrayList<NodeRef>();
          AlfrescoTransactionSupport.bindListener(this);
          AlfrescoTransactionSupport.bindResource(NODE_KEY, nodeRefs);
        }
        nodeRefs.add(nodeRef);
    }

    @Override
    public void beforeCommit(boolean readOnly)
    {
        List<NodeRef> nodeRefs = AlfrescoTransactionSupport.getResource(NODE_KEY);
        if (nodeRefs != null) {
            for (NodeRef nodeRef : nodeRefs) {
                if (nodeService.exists(nodeRef)) {
                    String fileName = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
                    LOGGER.debug("beforeCommit start : " + fileName + ", readonly : " + readOnly);
                    actionService.executeAction(actionService.createAction("virus-scan-action"), nodeRef);
                    LOGGER.debug("beforeCommit end : " + fileName);
                }
            }
        }
    }
}
