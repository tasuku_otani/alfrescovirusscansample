package jp.aegif.alfresco.sample.action.executer;

import org.alfresco.error.AlfrescoRuntimeException;

public class VirusDetectException extends AlfrescoRuntimeException {

    public VirusDetectException(String msgId) {
        super(msgId);
    }

    public VirusDetectException(String msgId, Throwable cause)
    {
        super(msgId, cause);
    }

    public VirusDetectException(String msgId, Object[] params)
    {
        super(msgId, params);
    }

    public VirusDetectException(String msgId, Object[] msgParams, Throwable cause)
    {
        super(msgId, msgParams, cause);
    }
}
