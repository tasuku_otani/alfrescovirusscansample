package jp.aegif.alfresco.sample.action.executer;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.TempFileProvider;
import org.alfresco.util.exec.RuntimeExec;
import org.alfresco.util.exec.RuntimeExec.ExecutionResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VirusScanActionExecuter extends ActionExecuterAbstractBase {

    private static final Log LOGGER = LogFactory.getLog(VirusScanActionExecuter.class);

    private ContentService contentService;
    private FileFolderService fileFolderService;
    private RuntimeExec virusScanCommand;

    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    public void setVirusScanCommand(RuntimeExec virusScanCommand) {
        this.virusScanCommand = virusScanCommand;
    }

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        String fileName = (String) fileFolderService.getFileInfo(nodeRef).getName();
        LOGGER.debug("Virus scan start : " + fileName);

        // Output content to temporary file
        ContentReader contentReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
        File targetFile = null;
        try {
            targetFile = TempFileProvider.createTempFile(contentReader.getContentInputStream(), "virusscan_", ".bin");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // Execute virus scan
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("target", targetFile.getAbsolutePath());
        ExecutionResult result = virusScanCommand.execute(properties);

        // Check result and delete infected file.
        if (result.getExitValue() == 0) {
            LOGGER.debug("Virus not detected : " + fileName);
        } else if (!result.getSuccess()) {
            fileFolderService.delete(nodeRef);
            if (targetFile.exists()) {
                targetFile.delete();
            }
            throw new VirusDetectException("Virus detected and cleaned up : " + fileName);
        } else {
            LOGGER.error("Unable to scan : " + result);
        }
}

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramDefs) {
        // Do nothing
    }

}
